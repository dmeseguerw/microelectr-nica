#!  usr/bin/env

#Daniel Meseguer Wong
#B74767
#Tarea 5

import glob
import pandas as pd
import re

data = []
columns = ['Module Name', 'Syn Mode', 'Clk Domain', 'Logic Levels','WNS','TNS','Comb Cell Count', 'Seq Cell Count','Comb Area', 'Seq Area', 'Design Area', '%Area Impr']
text_files = glob.glob("hier/*.rpt")
notclk = ["'COMBPATH'", "'IN2REG'","'REG2OUT'"]
synmode = 'hier'

#El while sirve para recorrer en primera instancia los archivos del directorio hier, y luego, cuando run es 1, recorre los archivos
#del directorio flat.
run = 0
while run<2:

    #Este for es donde se revisan todos los archivos
    for file in text_files:
        comb_count = None
        seq_count = None
        comb_area = None
        seq_area = None
        file_name = file

    #Con este with se calculan las areas y la cantidad de celdas secuenciales y combinacionales.
        with open(file) as f0:
            path_data = {}
            whole_file = []
            for line in f0:
                comb_count_re = re.search(r'^\s\sCombinational+\s+Cell+\s+Count:*\s+(\S+)',line)
                if comb_count_re:
                    comb_count = comb_count_re.group(1)

                seq_count_re = re.search(r'^\s\sSequential+\s+Cell+\s+Count:*\s+(\S+)',line)
                if seq_count_re:
                    seq_count = seq_count_re.group(1)

                comb_area_re = re.search(r'^\s\sCombinational\s+Area:*\s+(\S+)',line)
                if comb_area_re:
                    comb_area = comb_area_re.group(1)

                seq_area_re = re.search(r'^\s\sNoncombinational\s+Area:\s',line)

                if (seq_area == "  Noncombinational Area:\n"):
                    seq_area_re = re.search(r'^\s*\s+(\S+)', line)
                    seq_area_real = seq_area_re.group(1)
                if seq_area_re:
                    seq_area = seq_area_re.group(0)

                design_area_re = re.search(r'^\s\sDesign\s+Area:*\s+(\S+)',line)
                if design_area_re:
                    design_area = float(design_area_re.group(1))
                    

    # Se reemplaza el path de los archivos a leer para poder leer los archivos de la sintesis flat
    # y de esta forma comparar las dos areas para obtener el porcentaje de mejora de area de la sintesis flat con respecto a
    # la jerarquica.
        if (run==0):
            flat_file = file.replace('hier','flat')
            with open(flat_file) as f1:
                for line in f1:
                    flat_design_area_re = re.search(r'^\s\sDesign\s+Area:*\s+(\S+)',line)
                    if flat_design_area_re:
                        flat_design_area = float(flat_design_area_re.group(1))
                        area = ((design_area-flat_design_area)/design_area)*100
                    
        # Con este with se calculan los datos de cada clock domain, es decir, los logic levels, clk domains, WNS y TNS
        with open(file) as f:
            path_data = {}
            save_clk = None
            clk_domain = None
            logic_levels = None
            wns = None
            whole_file = []
            path_data[columns[6]] = comb_count
            path_data[columns[7]] = seq_count
            path_data[columns[8]] = comb_area
            path_data[columns[9]] = seq_area_real
            path_data[columns[10]] = design_area
            if (run==0):
                path_data[columns[11]] = area

            for line in f:
    
    ########################################################################################
    ##############CALCULO DE CLOCK DOMAINS#####################################################

                clk_domain_re = re.search(r'^\s\sTiming\s+Path\s+Group\s+(\S+)', line)
            
                if clk_domain_re:
                    clk_domain = clk_domain_re.group(1)
                    if clk_domain not in notclk:
                        path_data[columns[2]] = clk_domain
                        path_data[columns[0]] = module
                        path_data[columns[1]] = synmode
                        
    ########################################################################################
    ###########################CALCULO DE LOGIC LEVELS#############################################
                logic_levels_re = re.search(r'^\s\s+Levels+\s+of+\s+Logic:*\s+(\S+)', line)

                if logic_levels_re:
                    if clk_domain not in notclk:
                        logic_levels = logic_levels_re.group(1)
                        path_data[columns[3]] = logic_levels

    ########################################################################################
     ########################CALCULO DE WNS########################################################
                wns_re = re.search(r'^\s\s+Critical+\s+Path+\s+Slack:*\s+(\S+)',line)

                if wns_re:
                    if clk_domain not in notclk:
                        wns = wns_re.group(1)
                        path_data[columns[4]] = wns

    ########################################################################################
    ########################CALCULO DE TNS###################################################
                tns_re = re.search(r'^\s\s+Total+\s+Negative+\s+Slack:*\s+(\S+)',line)

                if tns_re:
                    if clk_domain not in notclk:
                        tns = tns_re.group(1)
                        path_data[columns[5]] = tns
                        data.append(path_data)
                        path_data = {}
                        
    ########################################################################################
    #######################NOMBRE DE MODULO QUE ES EL MISMO PARA TODOS LOS CLK DOMAINS###########
                module_re = re.search(r'^Design\s:\s(\S+)',line)
                if module_re:
                    module = module_re.group(1)

    # ########################################################################################

    text_files = glob.glob('flat/*.rpt')
    synmode = 'flat'
    run = run+1


    ###############FIN DEL WHILE##################################################




#########OBTENCION DE ARCHIVO .CSV Y ORDEN ##############################################

table = pd.DataFrame.from_records(data, columns = columns)
table['Design Area'] = pd.to_numeric(table['Design Area'])
table = table.sort_values(['Module Name','Syn Mode','Clk Domain'])
table.to_csv('Tarea5.csv', index = False)
