#!  usr/bin/env

import glob
import pandas as pd
import re

data = []
startpoint = None
endclock = None
columns = ["File", "Path Group", "Start Point", "End Point", "Start Clock", "End Clock", "Uncertainty", "Insertion Delay", "Period", "Slack"]

text_files = glob.glob("*.txt")

for file in text_files:
    in_startp = None
    with open(file) as f:
        
        path_data = {}

        for line in f:

            startpoint_re = re.search(r'^\s*Startpoint:\s+(\S+)', line)

            if startpoint_re:
                startpoint = startpoint_re.group(1)
                path_data[columns[2]] = startpoint
                in_startp = True

            endpoint_re = re.search(r'^\s*Endpoint:\s+(\S+)' , line)
            endpoint = None

            if endpoint_re:
                path_data[columns[3]] = endpoint_re.group(1)

            clock_re = re.search( r'clocked\s+by\s+(\w+)', line)
            if clock_re:
                if in_startp is not None:
                    path_data[columns[4]] = clock_re.group(1)
                    in_startp = None
                else:
                    endclock = clock_re.group(1)
                    path_data[columns[5]] = endclock

            pathgroup_re = re.search(r'^\s*Path\s+Group:\s+(\S+)', line)

            if pathgroup_re:
                path_data[columns[1]] = pathgroup_re.group(1)

            insertdelay_re = re.search(r'^\s*%s/\w+\s+\(.*\s+(\S+)\s+\w$' % startpoint, line)

            if insertdelay_re:
                path_data[columns[7]] = insertdelay_re.group(1)
            
            period = None
            period_re = re.search(r'^\s*clock\s+%s\s+\(.*\s+(\S+)$' % endclock, line)

            if period_re:
                period = period_re.group(1)
                if float(period) > 0:
                    path_data[columns[8]] = period

            uncertainty_re = re.search(r'uncertainty\s+(\S+)', line)
            
            if uncertainty_re:
                path_data[columns[6]] = uncertainty_re.group(1)

            slack_re = re.search(r'\s*slack\s+\(.*\)\s+(\S+)', line)

            if slack_re:
                path_data[columns[9]] = slack_re.group(1)
                path_data[columns[0]] = file

                data.append(path_data)
                path_data = {}

table = pd.DataFrame.from_records(data, columns = columns)
table.to_csv('Tarea4.csv', index = False)