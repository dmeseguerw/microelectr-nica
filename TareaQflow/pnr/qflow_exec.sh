#!/bin/tcsh -f
#-------------------------------------------
# qflow exec script for project /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr
#-------------------------------------------

/usr/local/share/qflow/scripts/synthesize.sh /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr/source/mux21.v || exit 1
/usr/local/share/qflow/scripts/placement.sh -d /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
# /usr/local/share/qflow/scripts/opensta.sh  /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
/usr/local/share/qflow/scripts/vesta.sh -a /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
/usr/local/share/qflow/scripts/router.sh /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
# /usr/local/share/qflow/scripts/opensta.sh  -d /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
/usr/local/share/qflow/scripts/vesta.sh -a -d /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
/usr/local/share/qflow/scripts/migrate.sh /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
/usr/local/share/qflow/scripts/drc.sh /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
/usr/local/share/qflow/scripts/lvs.sh /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
/usr/local/share/qflow/scripts/gdsii.sh /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
/usr/local/share/qflow/scripts/cleanup.sh /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
# /usr/local/share/qflow/scripts/display.sh /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr mux21 || exit 1
