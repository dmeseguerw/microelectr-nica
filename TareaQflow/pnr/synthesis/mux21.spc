*SPICE netlist created from BLIF module mux21 by blif2BSpice
.include /usr/local/share/qflow/tech/osu018/osu018_stdcells.sp
.subckt mux21 vdd gnd clk reset in1[0] in1[1] in1[2] in1[3] in1[4] in1[5] in1[6] in1[7] in1[8] in1[9] in1[10] in1[11] in1[12] in1[13] in1[14] in1[15] in2[0] in2[1] in2[2] in2[3] in2[4] in2[5] in2[6] in2[7] in2[8] in2[9] in2[10] in2[11] in2[12] in2[13] in2[14] in2[15] selector mux_out[0] mux_out[1] mux_out[2] mux_out[3] mux_out[4] mux_out[5] mux_out[6] mux_out[7] mux_out[8] mux_out[9] mux_out[10] mux_out[11] mux_out[12] mux_out[13] mux_out[14] mux_out[15] 
XINVX8_1 vdd gnd reset _1_ INVX8
XMUX2X1_1 selector vdd gnd _2_ in1[0] in2[0] MUX2X1
XNOR2X1_1 vdd _2_ gnd _0_[0] _1_ NOR2X1
XMUX2X1_2 selector vdd gnd _3_ in1[1] in2[1] MUX2X1
XNOR2X1_2 vdd _3_ gnd _0_[1] _1_ NOR2X1
XMUX2X1_3 selector vdd gnd _4_ in1[2] in2[2] MUX2X1
XNOR2X1_3 vdd _4_ gnd _0_[2] _1_ NOR2X1
XMUX2X1_4 selector vdd gnd _5_ in1[3] in2[3] MUX2X1
XNOR2X1_4 vdd _5_ gnd _0_[3] _1_ NOR2X1
XMUX2X1_5 selector vdd gnd _6_ in1[4] in2[4] MUX2X1
XNOR2X1_5 vdd _6_ gnd _0_[4] _1_ NOR2X1
XMUX2X1_6 selector vdd gnd _7_ in1[5] in2[5] MUX2X1
XNOR2X1_6 vdd _7_ gnd _0_[5] _1_ NOR2X1
XMUX2X1_7 selector vdd gnd _8_ in1[6] in2[6] MUX2X1
XNOR2X1_7 vdd _8_ gnd _0_[6] _1_ NOR2X1
XMUX2X1_8 selector vdd gnd _9_ in1[7] in2[7] MUX2X1
XNOR2X1_8 vdd _9_ gnd _0_[7] _1_ NOR2X1
XMUX2X1_9 selector vdd gnd _10_ in1[8] in2[8] MUX2X1
XNOR2X1_9 vdd _10_ gnd _0_[8] _1_ NOR2X1
XMUX2X1_10 selector vdd gnd _11_ in1[9] in2[9] MUX2X1
XNOR2X1_10 vdd _11_ gnd _0_[9] _1_ NOR2X1
XMUX2X1_11 selector vdd gnd _12_ in1[10] in2[10] MUX2X1
XNOR2X1_11 vdd _12_ gnd _0_[10] _1_ NOR2X1
XMUX2X1_12 selector vdd gnd _13_ in1[11] in2[11] MUX2X1
XNOR2X1_12 vdd _13_ gnd _0_[11] _1_ NOR2X1
XMUX2X1_13 selector vdd gnd _14_ in1[12] in2[12] MUX2X1
XNOR2X1_13 vdd _14_ gnd _0_[12] _1_ NOR2X1
XMUX2X1_14 selector vdd gnd _15_ in1[13] in2[13] MUX2X1
XNOR2X1_14 vdd _15_ gnd _0_[13] _1_ NOR2X1
XMUX2X1_15 selector vdd gnd _16_ in1[14] in2[14] MUX2X1
XNOR2X1_15 vdd _16_ gnd _0_[14] _1_ NOR2X1
XMUX2X1_16 selector vdd gnd _17_ in1[15] in2[15] MUX2X1
XNOR2X1_16 vdd _17_ gnd _0_[15] _1_ NOR2X1
XBUFX2_1 vdd gnd _18_[0] mux_out[0] BUFX2
XBUFX2_2 vdd gnd _18_[1] mux_out[1] BUFX2
XBUFX2_3 vdd gnd _18_[2] mux_out[2] BUFX2
XBUFX2_4 vdd gnd _18_[3] mux_out[3] BUFX2
XBUFX2_5 vdd gnd _18_[4] mux_out[4] BUFX2
XBUFX2_6 vdd gnd _18_[5] mux_out[5] BUFX2
XBUFX2_7 vdd gnd _18_[6] mux_out[6] BUFX2
XBUFX2_8 vdd gnd _18_[7] mux_out[7] BUFX2
XBUFX2_9 vdd gnd _18_[8] mux_out[8] BUFX2
XBUFX2_10 vdd gnd _18_[9] mux_out[9] BUFX2
XBUFX2_11 vdd gnd _18_[10] mux_out[10] BUFX2
XBUFX2_12 vdd gnd _18_[11] mux_out[11] BUFX2
XBUFX2_13 vdd gnd _18_[12] mux_out[12] BUFX2
XBUFX2_14 vdd gnd _18_[13] mux_out[13] BUFX2
XBUFX2_15 vdd gnd _18_[14] mux_out[14] BUFX2
XBUFX2_16 vdd gnd _18_[15] mux_out[15] BUFX2
XDFFPOSX1_1 vdd _0_[0] gnd _18_[0] clk DFFPOSX1
XDFFPOSX1_2 vdd _0_[1] gnd _18_[1] clk DFFPOSX1
XDFFPOSX1_3 vdd _0_[2] gnd _18_[2] clk DFFPOSX1
XDFFPOSX1_4 vdd _0_[3] gnd _18_[3] clk DFFPOSX1
XDFFPOSX1_5 vdd _0_[4] gnd _18_[4] clk DFFPOSX1
XDFFPOSX1_6 vdd _0_[5] gnd _18_[5] clk DFFPOSX1
XDFFPOSX1_7 vdd _0_[6] gnd _18_[6] clk DFFPOSX1
XDFFPOSX1_8 vdd _0_[7] gnd _18_[7] clk DFFPOSX1
XDFFPOSX1_9 vdd _0_[8] gnd _18_[8] clk DFFPOSX1
XDFFPOSX1_10 vdd _0_[9] gnd _18_[9] clk DFFPOSX1
XDFFPOSX1_11 vdd _0_[10] gnd _18_[10] clk DFFPOSX1
XDFFPOSX1_12 vdd _0_[11] gnd _18_[11] clk DFFPOSX1
XDFFPOSX1_13 vdd _0_[12] gnd _18_[12] clk DFFPOSX1
XDFFPOSX1_14 vdd _0_[13] gnd _18_[13] clk DFFPOSX1
XDFFPOSX1_15 vdd _0_[14] gnd _18_[14] clk DFFPOSX1
XDFFPOSX1_16 vdd _0_[15] gnd _18_[15] clk DFFPOSX1
XFILL_0_0_0 vdd gnd FILL
XFILL_0_0_1 vdd gnd FILL
XFILL_0_1_0 vdd gnd FILL
XFILL_0_1_1 vdd gnd FILL
XFILL_1_1 vdd gnd FILL
XFILL_1_0_0 vdd gnd FILL
XFILL_1_0_1 vdd gnd FILL
XFILL_1_1_0 vdd gnd FILL
XFILL_1_1_1 vdd gnd FILL
XFILL_2_0_0 vdd gnd FILL
XFILL_2_0_1 vdd gnd FILL
XFILL_2_1_0 vdd gnd FILL
XFILL_2_1_1 vdd gnd FILL
XFILL_3_1 vdd gnd FILL
XFILL_3_2 vdd gnd FILL
XFILL_3_3 vdd gnd FILL
XFILL_3_0_0 vdd gnd FILL
XFILL_3_0_1 vdd gnd FILL
XFILL_3_1_0 vdd gnd FILL
XFILL_3_1_1 vdd gnd FILL
XFILL_4_1 vdd gnd FILL
XFILL_4_2 vdd gnd FILL
XFILL_4_3 vdd gnd FILL
.ends mux21
 
