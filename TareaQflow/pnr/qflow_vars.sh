#!/bin/tcsh -f
#-------------------------------------------
# qflow variables for project /home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr
#-------------------------------------------

set projectpath=/home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr
set techdir=/usr/local/share/qflow/tech/osu018
set sourcedir=/home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr/source
set synthdir=/home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr/synthesis
set layoutdir=/home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr/layout
set techname=osu018
set scriptdir=/usr/local/share/qflow/scripts
set bindir=/usr/local/share/qflow/bin
set logdir=/home/dmeseguerw/Desktop/II-Semestre-2019/Micro-2019/TareaQflow/pnr/log
#-------------------------------------------

