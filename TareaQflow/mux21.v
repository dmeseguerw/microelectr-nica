module mux21 #(
    parameter WIDTH     = 16,
    parameter SEL_WIDTH = 1
) (
    input clk, reset,
    input [WIDTH-1:0] in1, in2,
    input [SEL_WIDTH-1:0] selector,
    output reg [WIDTH-1:0] mux_out
);

always@(posedge clk) begin
    if (~reset) begin
        mux_out <= 0;
    end
    else begin
    mux_out <= selector ? in1 : in2;
    end
end

endmodule
